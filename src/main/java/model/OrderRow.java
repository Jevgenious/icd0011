package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"id", "ordersId"})

public class OrderRow {

    private Long id;
    private String itemName;

    @NotNull(message = "quantity must not be null")
    @Min(1)
    private Integer quantity;

    @NotNull(message = "price must not be null")
    @Min(1)
    private Integer price;

    private Long ordersId;
}
