package api;

import model.Order;
import model.OrderRow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import util.FileUtil;


import java.sql.*;
import java.util.List;

@Repository
public class OrderDao {
    private JdbcTemplate template;

    @Autowired
    public OrderDao(JdbcTemplate template){
        this.template = template;
    }


    public List<Order> getOrders(){
        String sql = FileUtil.readFileFromClasspath("find_orders.sql");
            var orderMapper = new BeanPropertyRowMapper<>(Order.class);
            List<Order> orders = template.query(sql, orderMapper);
        for (Order row : orders) {
            String sql2 = FileUtil.readFileFromClasspath("find_order_orderRow_byId.sql");
            var orderRowMapper = new BeanPropertyRowMapper<>(OrderRow.class);
            List<OrderRow> orderRows = template.query(sql2, new Object[]{row.getId()}, orderRowMapper);
            row.setOrderRows(orderRows);
        }
            return orders;
    }

    public Order insertOrder(Order order){
        SqlParameterSource dataOrder = new MapSqlParameterSource()
                .addValue("order_number", order.getOrderNumber());
        Number idOrder = new SimpleJdbcInsert(template)
                .withTableName("orders")
                .usingGeneratedKeyColumns("id")
                .executeAndReturnKey(dataOrder);
        order.setId(idOrder.longValue());
        List<OrderRow> orderRowList = order.getOrderRows();
        if (orderRowList != null) {
            insertOrderRow(orderRowList, order.getId());
        }
        return new Order(order.getId(),
                order.getOrderNumber(),
                order.getOrderRows());
    }
    private class OrderRowMapper implements RowMapper<OrderRow> {
        @Override
        public OrderRow mapRow(ResultSet rs, int rowNum)
            throws SQLException {

            OrderRow orderRow = new OrderRow();
                orderRow.setItemName(rs.getString("item_name"));
                orderRow.setQuantity(rs.getInt("quantity"));
                orderRow.setPrice(rs.getInt("price"));
            return orderRow;
        }
    }

    public Order getOrderById(Long id){
        String sql = FileUtil.readFileFromClasspath("find_order_byId.sql");
        var orderMapper = new BeanPropertyRowMapper<>(Order.class);
        Order order = template.queryForObject(sql, new Object[]{id}, orderMapper);
        order.setOrderRows(getOrderRowById(order));
        return order;
    }

    public List<OrderRow> getOrderRowById(Order order){
        String sql = FileUtil.readFileFromClasspath("find_order_orderRow_byId.sql");
        return template.query(sql, new Object[]{order.getId()}, new OrderRowMapper());

    }

    public void deleteOrder(Long id){
        String sql = FileUtil.readFileFromClasspath("delete_order.sql");
        template.update(sql, new Object[]{id});
    }

    public void insertOrderRow(List<OrderRow> orderRowList, Long orderId){
        for (OrderRow row : orderRowList) {
            SqlParameterSource dataOrderRow = new MapSqlParameterSource()
                    .addValue("item_name", row.getItemName())
                    .addValue("quantity", row.getQuantity())
                    .addValue("price", row.getPrice())
                    .addValue("orders_id", orderId);

            Number idOrderRow = new SimpleJdbcInsert(template)
                    .withTableName("order_rows")
                    .usingGeneratedKeyColumns("id")
                    .executeAndReturnKey(dataOrderRow);
            row.setId(idOrderRow.longValue());
            row.setOrdersId(orderId);

        }
    }
}
