package config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import util.FileUtil;
import javax.annotation.PostConstruct;



import javax.sql.DataSource;
@Component
public class InitDb {
    private JdbcTemplate template;

    @Autowired
    public InitDb(JdbcTemplate template){
        this.template = template;
    }

    @PostConstruct
    public void initDb(){
        String sql1 = FileUtil.readFileFromClasspath("db_init.sql");
        //String sql2 = FileUtil.readFileFromClasspath("db_init_data.sql");
        template.update(sql1);
        //template.update(sql2);
    }


}
