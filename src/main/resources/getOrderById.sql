SELECT orders.id,
       orders.order_number,
       order_rows.quantity,
       order_rows.id,
       order_rows.orders_id,
       order_rows.item_name,
       order_rows.price
FROM orders LEFT JOIN order_rows ON order_rows.orders_id = orders.id
WHERE orders.id = ?